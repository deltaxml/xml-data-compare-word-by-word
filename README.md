# Word by Word


For details about this sample see our [web pages.](https://docs.deltaxml.com/xdc/latest/samples/word-by-word)

Using the file config-empty.xml will give the default value of 'on' for the word-by-word feature for the whole comparison.  

Using config-wbw-false.xml will process the comparison with word-by-word switched off.  

Finally using config-wbw-specific will have word-by-word switched off except for the `extra` element.  


## REST request:

Replace {LOCATION} below with your location of the download. 



```
<compare>
    <inputA xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:type="file">
        <path>{LOCATION}/addressesA.xml</path>
    </inputA>
    <inputB xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:type="file">
        <path>{LOCATION}/addressesB.xml</path>
    </inputB>
    <configuration xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:type="file">
        <path>{LOCATION}/config-wbw-specific.xml</path>
    </configuration>
</compare>
```



See [XML Data Compare REST User Guide](https://docs.deltaxml.com/xdc/latest/rest-user-guide) for how to run the comparison using REST.

